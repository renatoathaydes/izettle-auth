package com.athaydes.izettle.api.authentication

import com.athaydes.izettle.api.storage.NonceStorage
import com.athaydes.izettle.api.storage.UserStorage
import com.athaydes.izettle.data.PersistedUser
import com.athaydes.izettle.data.User
import spock.lang.Specification

import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder

class AuthenticationServiceSpec extends Specification {


    def "AuthorizationService presents user a login page when user is not authenticated"() {
        given: 'No-op mocks for user and nonce services'
        def nonceStorage = Mock( NonceStorage )
        def userStorage = Mock( UserStorage )
        def appConfig = new ClientConfig( 'id', 'secret', URI.create( "http://redirect" ) )

        and: 'An AuthenticationService'
        def service = new AuthenticationService( nonceStorage, userStorage, appConfig )

        when: 'The user starts authentication without presenting an authorization header'
        def response = service.startAuthentication()

        then: 'The user should be presented with the login screen'
        response instanceof LoginView

    }

    def "AuthorizationService correctly authenticates a user if the presented credentials are correct"() {
        given: 'Valid credentials'
        def userCredentials = new UserCredentials( 'me', 'pass' )

        and: 'A Mock UserStorage that can verify the valid credentials of the user'
        def persistedUser = new PersistedUser( new User( userCredentials.username, '333' ), 'user-id' )

        def userStorage = Mock( UserStorage )
        1 * userStorage.getByCredentials( userCredentials ) >> persistedUser

        def appConfig = new ClientConfig( 'id', 'secret', URI.create( "http://redirect" ) )

        and: 'A Mock for nonce Storage that creates a nonce for the valid user'
        def nonceStorage = Mock( NonceStorage )
        1 * nonceStorage.createNonce( userCredentials.username ) >> 'the-nonce'

        and: 'An AuthenticationService'
        def service = new AuthenticationService( nonceStorage, userStorage, appConfig )

        when: 'The user tries to authenticate by presenting valid credentials'
        def response = service.authenticate( userCredentials.username, userCredentials.password,
                'id', 'hi', '' )

        then: 'The response status code should be 303 (redirect)'
        assert response instanceof Response
        response.status == 303

        and: 'The response should contain a Location header pointing to the configured redirect URI'
        response.metadata == [ Location: [ UriBuilder.fromUri( 'http://redirect/?code=the-nonce' ).build() ] ]

        when: 'The user starts authentication without presenting an authorization header'
        response = service.authenticate( 'joe', 'doe', 'id', null, null )

        then: 'The user should be presented with the login screen'
        response instanceof LoginView

    }

}