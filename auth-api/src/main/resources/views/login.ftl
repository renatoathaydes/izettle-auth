<#-- @ftlvariable name="" type="com.athaydes.izettle.api.authentication.LoginView" -->
<html>
<head>
    <title>Log in</title>
    <style>
        .error {
            color: red;
        }

        .info {
            color: darkgreen;
        }

        .lower {
            padding-top: 15px;
        }
    </style>
</head>
<body>
<h1>Renato Athaydes Authorization Server</h1>
<h2>Please, enter your credentials to log in:</h2>
<#if infoMessage??>
<div class="info">${infoMessage?html}</div>
</#if>

<form action="/authenticate" method="post">
    <input type="hidden" name="clientId" value="${clientId?html}"/>
    <input type="hidden" name="scope" value="${scope?html}"/>
    <input type="hidden" name="state" value="${state?html}"/>
    <div>
        <label for="username">Username:</label>
    </div>
    <div>
        <input type="text" id="username" name="username" value="${user?html}"/>
    </div>
    <div>
        <label for="password">Password</label>
    </div>
    <div>
        <input type="password" id="password" name="password"/>
    </div>
    <#if errorMessage??>
        <div class="error">${errorMessage?html}</div>
    </#if>
    <div class="lower">
        <button type="submit">Log in</button>
    </div>
</form>
</body>
</html>