package com.athaydes.izettle.api.error

import io.dropwizard.jersey.validation.ValidationErrorMessage
import io.dropwizard.views.View
import io.dropwizard.views.ViewRenderer
import java.io.OutputStream
import java.lang.reflect.Type
import java.util.Locale
import java.util.ServiceLoader
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Context
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.MessageBodyWriter

/**
 * HTML error view.
 */
class ErrorView(val status: Response.Status,
                val errors: List<String>) : View("/views/html-error.ftl")

/**
 * HTML body writer for [ValidationErrorMessage] instances.
 *
 * This is necessary because there doesn't seem to be a way to intercept request validation errors
 * in any other way.
 */
object ValidationErrorMessageBodyWriter : MessageBodyWriter<ValidationErrorMessage> {

    override fun getSize(t: ValidationErrorMessage?, type: Class<*>?,
                         genericType: Type?,
                         annotations: Array<out Annotation>?,
                         mediaType: MediaType?) = -1L // as recommended in JAX-RS 2.0

    override fun isWriteable(type: Class<*>?,
                             genericType: Type?,
                             annotations: Array<out Annotation>?,
                             mediaType: MediaType?) =
            type == ValidationErrorMessage::class.java &&
                    mediaType == MediaType.TEXT_HTML_TYPE

    override fun writeTo(errorMessage: ValidationErrorMessage,
                         type: Class<*>?, genericType:
                         Type?, annotations:
                         Array<out Annotation>?,
                         mediaType: MediaType?,
                         httpHeaders: MultivaluedMap<String, Any>,
                         entityStream: OutputStream) {
        val viewRederer = ServiceLoader.load(ViewRenderer::class.java).first()
        viewRederer.render(
                ErrorView(Response.Status.BAD_REQUEST, errorMessage.errors),
                Locale.ENGLISH, entityStream)
    }

}

class WebApplicationExceptionMapper : ExceptionMapper<WebApplicationException> {

    @Context
    private var headers: HttpHeaders? = null

    override fun toResponse(exception: WebApplicationException): Response {
        val mediaTypes = headers?.acceptableMediaTypes?.toSet() ?: emptySet()
        return when {
            MediaType.TEXT_HTML_TYPE in mediaTypes -> htmlResponse(exception)
            MediaType.APPLICATION_JSON_TYPE in mediaTypes -> jsonResponse(exception)
            else -> plainTextResponse(exception)
        }
    }

    private fun plainTextResponse(exception: WebApplicationException) =
            Response.status(exception.response.status)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                    .entity("An error has occurred: ${exception.message}")
                    .build()

    private fun jsonResponse(exception: WebApplicationException) =
            Response.status(exception.response.status)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                    .entity("{ \"errors\": [ \"${exception.message}\" ] }")
                    .build()

    private fun htmlResponse(exception: WebApplicationException): Response {
        val status = Response.Status.fromStatusCode(exception.response.status)
        val defaultErrorMessage = status.reasonPhrase

        return Response.status(exception.response.status)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_HTML)
                .entity(ErrorView(status, listOf(exception.message ?: defaultErrorMessage)))
                .build()
    }
}
