package com.athaydes.izettle.api.endpoints

import com.athaydes.izettle.api.ServerSettings
import com.athaydes.izettle.api.authentication.AuthenticationService
import com.athaydes.izettle.api.authentication.ClientConfig
import com.athaydes.izettle.api.error.JsonErrorView
import com.athaydes.izettle.api.storage.StorageException
import com.athaydes.izettle.api.storage.UserStorage
import com.athaydes.izettle.data.User
import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.views.View
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.ws.rs.Consumes
import javax.ws.rs.FormParam
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

data class RegistrationView(val errorMessage: String? = null) :
        View("/views/register.ftl")

// no validation done in this Object itself because Dropwizard fails to give a proper error message if something fails
data class NewUser(@JsonProperty("username") val username: String?,
                   @JsonProperty("phone") val phone: String?,
                   @JsonProperty("password") val plainTextPassword: String?)

/**
 * The registration endpoint.
 */
@Path(ServerSettings.Endpoints.REGISTRATION)
class RegisterEndpoint(val userStorage: UserStorage,
                       val clientConfig: ClientConfig) {

    val log: Logger = LoggerFactory.getLogger(RegisterEndpoint::class.java)

    @GET
    @Produces(MediaType.TEXT_HTML)
    fun showRegistrationForm() = RegistrationView()

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    fun createFormUser(@FormParam("username") username: String?,
                       @FormParam("phone") phone: String?,
                       @FormParam("password") plainTextPassword: String?): Any {
        // validate the parameters here - don't let Dropwizard do it because we need to give the user the
        // registration form back, only with error messages in it
        if (username == null || username.isBlank()) {
            return RegistrationView(errorMessage = "Please enter a username")
        }

        val user = User(username, phone ?: "")

        return try {
            log.info("Trying to create new user {}", user)
            userStorage.createUser(user, plainTextPassword ?: "")

            AuthenticationService.showLoginPage(
                    clientId = clientConfig.clientId,
                    inforMessage = "Congratulations! You are now registered, please login.")
        } catch (storageException: StorageException) {
            log.debug("Error on registration: {}", storageException.toString())
            RegistrationView(errorMessage = storageException.reason)
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    fun createJsonUser(newUser: NewUser): Response {

        // validate newUser here because Dropwizard validation doesn't give very good error messages
        val username = newUser.username

        if (username == null || username.isBlank()) {
            return jsonBadRequest("username is missing")
        }

        return try {
            log.info("Trying to create new user {}", newUser.username)
            val persistedUser = userStorage.createUser(
                    User(newUser.username, newUser.phone ?: ""),
                    newUser.plainTextPassword ?: "")

            Response.status(Response.Status.CREATED)
                    .entity(mapOf(
                            "user_id" to persistedUser.userId,
                            "phone" to persistedUser.user.phone,
                            "username" to persistedUser.user.username))
                    .build()
        } catch (storageException: StorageException) {
            log.debug("Error on registration: {}", storageException.toString())

            jsonBadRequest(storageException.reason)
        }
    }

    private fun jsonBadRequest(message: String): Response {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(JsonErrorView(Response.Status.BAD_REQUEST.statusCode, message))
                .build()
    }

}
