package com.athaydes.izettle.browser_tests

import com.athaydes.izettle.browser_tests.traits.CanUseWebsite
import com.athaydes.izettle.browser_tests.traits.RequiresRunningServer
import geb.spock.GebSpec

class LoginsEndpointBrowserTest extends GebSpec
        implements CanUseWebsite, RequiresRunningServer {

    def "User can see her own login information but not other's"() {
        given: 'A new user is created and logged in'
        def firstUser = createAppUserAndLogin()

        and: 'The user logs out and logs in again'
        login( firstUser )

        and: 'Another user is created and logged in'
        def otherUser = createAppUserAndLogin()
        assert firstUser.username != otherUser.username

        when: 'The other user goes to the /logins page'
        go LoginsDataPage.url

        then: 'The user should see only one login entry and the greeting should match his username'
        def page = at LoginsDataPage
        page.loginData.size() == 1
        page.greeting.text() == "Hello, ${otherUser.username}!"

        when: 'The first user logs back in'
        login( firstUser )

        and: 'The first user goes to the /logins page'
        go LoginsDataPage.url
        page = at LoginsDataPage

        then: 'The first user sees 3 login entries and the greeting should match her username'
        page.loginData.size() == 3
        page.greeting.text() == "Hello, ${firstUser.username}!"
    }

    def "User cannot go to the /logins page without authenticating"() {
        given: 'No user is logged in'
        logout()

        when: 'The /logins page is accessed'
        go LoginsDataPage.url

        then: 'The user is redirected to the Log In page'
        at LoginPage
    }

}
