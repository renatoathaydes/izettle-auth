package com.athaydes.izettle.simple_app

import com.athaydes.izettle.test.client.KnowsOAuthFlows
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import spark.Request
import spark.Response
import spark.Spark

import javax.annotation.Nullable
import javax.ws.rs.core.HttpHeaders
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

import static javax.ws.rs.core.MediaType.APPLICATION_JSON

/**
 * This is a very simple app that will listen to the OAuth callback which is called
 * by the authorization server when the user authenticates.
 *
 * It will accept the code from the OAuth server, exchange it for an access token,
 * then let the user make calls with the Auth API using the token.
 */
@CompileStatic
class SimpleClientApp implements KnowsOAuthFlows {

    static final private class OAuthSettings {
        static final String clientId = '8a0be41f7b70bdd49512'
        static final String clientSecret = '2a507f5da8c71ce91f3b2241744b9a7be501dee6'
    }

    private static final String ACCESS_COOKIE_NAME = 'access_cookie'

    private static final URI authenticateEndpoint = URI.create( 'http://localhost:8080/authenticate' )
    private static final URI loginsEndpoint = URI.create( 'http://localhost:8080/api/v1/logins' )

    /**
     * In order to hide the cookie from the HTTP client, we map all authenticated users's opaque access cookie
     * to an access token.
     *
     * In the real world, we might have to use a distributed key-value store for this and keep track of the client's
     * IP, for example.
     */
    private final Map<String, String> tokenByAccessCookie = [ : ]

    private final OkHttpClient httpClient = new OkHttpClient().newBuilder()
            .followRedirects( false )
            .followSslRedirects( false )
            .build()

    private final String indexPageHeader

    SimpleClientApp() {
        def jsOAuthClientSourceURL = getClass().getResource( '/js/simple-app.js' )

        if ( !jsOAuthClientSourceURL ) {
            throw new IllegalStateException( 'Could not load the OAuth JS Client source from /js/simple-app.js' )
        }

        indexPageHeader = createHeader( jsOAuthClientSourceURL.text )
    }

    @Override
    String getClientId() { OAuthSettings.clientId }

    @Override
    String getClientSecret() { OAuthSettings.clientSecret }

    void start() {
        Spark.port 8082

        // the OAuth server will redirect here on successful login
        // with a code we can use to get a token on behalf of the user...
        Spark.get( '/' ) { Request req, res -> entryPage( req ) }
        Spark.get( '/index' ) { Request req, res -> entryPage( req ) }
        Spark.get( '/oauth-callback' ) { Request req, Response res -> onOauthCallback( req, res ) }
        Spark.get( '/logins' ) { Request req, Response res -> getLoginData( req, res ) }
        Spark.get( '/logout' ) { Request req, Response res -> logout( req, res ) }
        Spark.get( '/stop' ) { Request req, Response res -> die( res ) }
    }

    def entryPage( Request request ) {
        def token = decodeToken( getAccessTokenFrom( request ) )
        if ( token ) {
            createIndexPage( indexPageHeader, '', '', token )
        } else {
            createIndexPage( indexPageHeader )
        }
    }

    def onOauthCallback( Request request, Response response ) {
        def code = request.queryParams( 'code' )

        if ( code ) {
            okhttp3.Response tokenResponse = exchangeCodeForToken( code )
            def body = tokenResponse.body()
            if ( isJsonOk( tokenResponse.code(), body ) ) {
                def jsonBody = new JsonSlurper().parse( body.byteStream() ) as Map
                if ( jsonBody[ 'access_token' ] ) {
                    def cookie = UUID.randomUUID().toString()
                    def tokenString = jsonBody[ 'access_token' ].toString()
                    tokenByAccessCookie[ cookie ] = tokenString
                    def token = decodeToken( tokenString )
                    return userGotCookie( cookie, token, response )
                }
            }

            return showError( body.string() )
        }

        def error = request.queryParams( 'error' ) ?: 'Unexpected request to oauth-callback,' +
                ' no code or error was provided'

        response.status( 400 )

        return showError( error )
    }

    private static boolean isJsonOk( int status, ResponseBody body ) {
        status == 200 && body.contentType().toString() == APPLICATION_JSON
    }

    def showError( error ) {
        createIndexPage( indexPageHeader, "<pre>$error</pre>" )
    }

    def userGotCookie( String cookie, Map token, Response response ) {
        response.cookie( ACCESS_COOKIE_NAME, cookie )
        return createIndexPage( indexPageHeader, '', 'You are logged in!', token )
    }

    def logout( Request request, Response response ) {
        response.cookie( ACCESS_COOKIE_NAME, null )
        createIndexPage( indexPageHeader, '', 'You are logged out!' )
    }

    static die( Response response ) {
        Thread.start {
            sleep 250 // to answer to the client
            Spark.stop()
        }
        return 'Goodbye!'
    }

    def getLoginData( Request request, Response response ) {
        // TODO the token might have to be refreshed!
        String token = getAccessTokenFrom( request )

        if ( token ) {
            okhttp3.Response loginsResponse = makeAuthorizedRequestTo( loginsEndpoint, token )

            def body = loginsResponse.body()
            if ( isJsonOk( loginsResponse.code(), body ) ) {
                Map jsonBody = new JsonSlurper().parse( body.byteStream() ) as Map
                def logins = jsonBody.logins
                if ( logins instanceof List ) {
                    return loginsAsHtmlData( logins as List, decodeToken( token ) )
                }
                return showError( 'Logins response does not have the expected format' )
            }

            return showError( 'Unexpected response from logins endpoint: ' + body.string() )
        } else {
            // user is not authorized, ask them to get back to us after authorizing
            response.redirect( authenticateEndpoint.toString() )
            return null
        }
    }

    private okhttp3.Response makeAuthorizedRequestTo( URI endpoint,
                                                      String token ) {
        def request = new okhttp3.Request.Builder()
                .header( HttpHeaders.AUTHORIZATION, "Bearer $token" )
                .url( endpoint.toURL() )
                .build()

        httpClient.newCall( request ).execute()
    }

    @Nullable
    private String getAccessTokenFrom( Request request ) {
        def cookie = request.cookie( ACCESS_COOKIE_NAME )
        if ( cookie ) {
            return tokenByAccessCookie[ cookie ]
        }
        return null
    }

    String loginsAsHtmlData( List data, Map token ) {
        final formatter = DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
                .withLocale( Locale.GERMAN )
                .withZone( ZoneId.systemDefault() )

        def displayTime = { time ->
            try {
                Instant instant = Instant.ofEpochMilli( ( Long.parseLong( time as String ) ) )
                return formatter.format( instant )
            } catch ( e ) {
                e.printStackTrace()
                return time.toString()
            }
        }

        def html = data.collect { item -> "<li>${displayTime( item )}</li>" }.join( '' )

        println "LOGIN DATA: $html"

        if ( html ) {
            return createIndexPage( indexPageHeader, '',
                    "<div>Your latest logins are:</div><ul>${html}</ul>", token )
        } else {
            createIndexPage( indexPageHeader, '',
                    '<div>No login data is available.</div>',
                    token )
        }
    }

    /**
     * Create a HTML head element with styles and the JS code embedded.
     * @param js
     * @return HTML head
     */
    static String createHeader( String js ) {
        """
        <head>
          <title>Simple App</title>
          <style>
            #error {
              color: red;
            }
            .very-large {
              font-size: 32;
            }
            #content {
              font-size: 24;
              padding-top: 20px;
              color: darkgreen;
            }
            .menu {
              min-height: 52px;
            }
            .menu-button {
              padding: 10px;
              background-color: lightgray;
              max-width: 150px;
              border: solid 1px;
              margin: 4px;
            }
            body {
              background: #FFEEFF;
              font-family: Helvetica;
            }
            h2 {
              background: #FFAAFF;
              padding: 50px;
              border-style: double;
            }
          </style>
          <script>$js</script>
        </head>
        """
    }

    /**
     * @return the single-page app.
     */
    static String createIndexPage( header,
                                   String error = '',
                                   String content = '',
                                   Map token = [ : ] ) {
        String topContent
        String menu = ''

        if ( token.sub ) {
            menu = "<div class='menu'><span class='menu-button'><a href='/logins'>Login Information</a></span>" +
                    "<span class='menu-button'><a href='/logout'>Logout</a></span></div>"
            topContent = "<div id='greeting'>Hello, ${token.sub}!</div>"
        } else {
            topContent = "<div class='very-large'>You don't seem to be logged in.</div>" +
                    "<div class='very-large'><a id='login-link' href='${authenticateEndpoint}'>Click here</a> to login.</div>"
        }

        """
        <html>
          $header
          <body>
            <h2>Simple Client App</h2>
            $menu
            $topContent
            <div id='error'>$error</div>
            <div id='content'>$content</div>
          </body>
        </html>
        """
    }

    static void main( args ) {
        new SimpleClientApp().start()
    }

}
