package com.athaydes.izettle.server.test

import com.google.common.net.HttpHeaders
import groovy.json.JsonSlurper
import okhttp3.FormBody
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okio.ByteString
import org.eclipse.jetty.http.HttpStatus
import spock.lang.Specification

import javax.ws.rs.core.UriBuilder

import static javax.ws.rs.core.MediaType.APPLICATION_JSON
import static javax.ws.rs.core.MediaType.TEXT_HTML

class RegistrationEndpointIntegrationTest extends Specification
        implements IntegrationTest {

    private final URI registrationEndpoint = URI.create( 'http://localhost:8080/registration' )

    def client = new OkHttpClient()

    @Override
    String getClientId() { TestConstants.clientId }

    @Override
    String getClientSecret() { TestConstants.clientSecret }

    def "Should be able to create a user with JSON"() {
        given: 'A JSON user'
        def user = '{"username":"joe", "phone": "504030304", "password": "joe45"}'

        when: 'the user is POSTED on the registration endpoint'
        def uriBuilder = UriBuilder.fromUri( registrationEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, APPLICATION_JSON )
                .post( jsonBody( user ) )
                .build()

        def response = client.newCall( request ).execute()

        then: 'The server responds with CREATED and gives back user information, including the ID'
        response.code() == HttpStatus.CREATED_201
        def jsonBody = response.body()
        jsonBody.contentType().toString() == APPLICATION_JSON

        def json = new JsonSlurper().parseText( jsonBody.string() ) as Map

        json.username == 'joe'
        json.phone == '504030304'
        json.user_id?.size() > 10
        json.size() == 3 // no more fields
    }

    def "Cannot create a user with JSON missing a username"() {
        given: 'A JSON user without username'
        def user = '{"phone": "504030304", "password": "anonymous"}'

        when: 'the user is POSTED on the registration endpoint'
        def uriBuilder = UriBuilder.fromUri( registrationEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, APPLICATION_JSON )
                .post( jsonBody( user ) )
                .build()

        def response = client.newCall( request ).execute()

        then: 'The server responds with a BAD REQUEST error and a JSON response'
        response.code() == HttpStatus.BAD_REQUEST_400
        def jsonBody = response.body()
        jsonBody.contentType().toString() == APPLICATION_JSON

        def json = new JsonSlurper().parseText( jsonBody.string() ) as Map

        json == [ code: HttpStatus.BAD_REQUEST_400, message: 'username is missing' ]
    }


    def "Should be able to create a user with a HTML FORM"() {
        when: 'A user HTML form is POSTED on the registration endpoint missing a username'
        def uriBuilder = UriBuilder.fromUri( registrationEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, TEXT_HTML )
                .post( new FormBody.Builder()
                .add( 'username', 'michael' )
                .add( 'phone', '233445334' )
                .add( 'password', 'jordan' ).build() )
                .build()

        def response = client.newCall( request ).execute()

        then: 'The server shows the user a login page'
        response.code() == HttpStatus.OK_200
        def htmlBody = response.body()
        htmlBody.contentType().toString() == TEXT_HTML

        def bodyContents = htmlBody.string()
        bodyContents.contains( '<title>Log in</title>' )
    }

    def "Cannot create a user with FORM missing a username"() {
        when: 'A user HTML form is POSTED on the registration endpoint missing a username'
        def uriBuilder = UriBuilder.fromUri( registrationEndpoint )

        def request = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, TEXT_HTML )
                .post( new FormBody.Builder()
                .add( 'phone', '233445334' )
                .add( 'password', 'hello' ).build() )
                .build()

        def response = client.newCall( request ).execute()

        then: 'The server responds with OK and the registration form containing an error'
        response.code() == HttpStatus.OK_200
        def htmlBody = response.body()
        htmlBody.contentType().toString() == TEXT_HTML

        def bodyContents = htmlBody.string()
        bodyContents.contains( '<title>Register</title>' )
        bodyContents.contains( '<div class="error">Please enter a username</div>' )
    }

    def "Cannot create two users with the same username"() {
        given: 'Two JSON users with the same username'
        def user1 = '{"username":"mark", "phone": "504030304", "password": "mark45"}'
        def user2 = '{"username":"mark", "password": "bbbb"}'

        when: 'the users are POSTED on the registration endpoint'
        def uriBuilder = UriBuilder.fromUri( registrationEndpoint )

        def request1 = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, APPLICATION_JSON )
                .post( jsonBody( user1 ) )
                .build()

        def request2 = new Request.Builder()
                .url( uriBuilder.build().toURL() )
                .header( HttpHeaders.ACCEPT, APPLICATION_JSON )
                .post( jsonBody( user2 ) )
                .build()

        def response1 = client.newCall( request1 ).execute()

        then: 'The server responds with CREATED on the first request'
        response1.code() == HttpStatus.CREATED_201
        def jsonBody1 = response1.body()
        jsonBody1.contentType().toString() == APPLICATION_JSON

        when: 'The same user is posted again'
        def response2 = client.newCall( request2 ).execute()

        then: 'The server gives a Bad Request error'
        response2.code() == HttpStatus.BAD_REQUEST_400
        def jsonBody2 = response2.body()
        jsonBody2.contentType().toString() == APPLICATION_JSON

        and: 'The error message reports the username already exists'
        def json = new JsonSlurper().parseText( jsonBody2.string() ) as Map

        json == [ code: HttpStatus.BAD_REQUEST_400, message: 'username already exists' ]

    }

    private static RequestBody jsonBody( String text ) {
        RequestBody.create( MediaType.parse( APPLICATION_JSON ), ByteString.of( text.bytes ) )
    }

}
