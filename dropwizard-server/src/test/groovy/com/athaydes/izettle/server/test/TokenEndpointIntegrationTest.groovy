package com.athaydes.izettle.server.test

import com.athaydes.izettle.test.client.CanAuthenticate
import com.athaydes.izettle.test.client.KnowsOAuthFlows
import groovy.json.JsonSlurper
import spock.lang.Specification

import javax.ws.rs.core.MediaType

class TokenEndpointIntegrationTest extends Specification
        implements CanAuthenticate, KnowsOAuthFlows, IntegrationTest {

    @Override
    String getClientId() { TestConstants.clientId }

    @Override
    String getClientSecret() { TestConstants.clientSecret }

    def "A Client should be able to exchange a valid code for an access token"() {
        when: 'A client requests authorization with the OAuth server'
        def response = authorize()

        then: 'The client is shown the login page'
        response.code() == 200
        def body = response.body()
        body.contentType().toString() == MediaType.TEXT_HTML
        def html = body.byteStream().withCloseable {
            new XmlSlurper().parse it
        }

        and: 'The login page contains the hidden data required to authorize'
        def hiddenInputs = html.depthFirst().findAll { it.name() == 'input' && it.'@type' == 'hidden' }
        hiddenInputs.size() == 3

        def clientId = hiddenInputs.find { it.@name == 'clientId' }?.@value?.text()
        def scope = hiddenInputs.find { it.@name == 'scope' }?.@value?.text()
        def state = hiddenInputs.find { it.@name == 'state' }?.@value?.text()

        clientId == TestConstants.clientId && scope == 'login_data' && state == ''

        when: 'The form is submitted via the authenticate endpoint with a valid user'
        response = authenticate( TestConstants.username, TestConstants.password, clientId, scope, state )

        then: 'The server should redirect the client with a code'
        def code = extractCodeFrom( response )
        code != null && code.size() > 10

        when: 'The code is sent to the token endpoint'
        response = exchangeCodeForToken( code )

        then: 'The response should be 200 with JSON'
        response.code() == 200
        def jsonBody = response.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON

        and: 'The JSON contents should have an access token'
        def json = new JsonSlurper().parseText( jsonBody.string() ) as Map
        json.access_token != null
        json.access_token.toString().size() > 10

        and: 'The JSON contents also have the token metadata'
        json.token_type == 'Bearer'
        json.expires_in == 3600
    }

    def "If a Client presents a fake nonce, it is not accepted by the server"() {
        when: 'A fake code is sent to the token endpoint'
        def response = exchangeCodeForToken( UUID.toString() )

        then: 'The client should get a bad request response with the "invalid_grant" error'
        response.code() == 400
        def jsonBody = response.body()

        jsonBody.contentType().toString() == MediaType.APPLICATION_JSON
        def json = new JsonSlurper().parseText( jsonBody.string() ) as Map

        json.error == 'invalid_grant'

    }

}
