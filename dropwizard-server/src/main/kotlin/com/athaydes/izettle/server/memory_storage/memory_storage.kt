package com.athaydes.izettle.server.memory_storage


import com.athaydes.izettle.api.authentication.UserCredentials
import com.athaydes.izettle.api.storage.LoginDataStorage
import com.athaydes.izettle.api.storage.NonceStorage
import com.athaydes.izettle.api.storage.StorageException
import com.athaydes.izettle.api.storage.UserStorage
import com.athaydes.izettle.api.token.TokenService
import com.athaydes.izettle.data.LoginData
import com.athaydes.izettle.data.PersistedUser
import com.athaydes.izettle.data.Token
import com.athaydes.izettle.data.User
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.jackson.Jackson
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import java.time.Duration
import java.time.Instant
import java.util.Base64
import java.util.UUID

class MemoryBasedTokenService(
        override val tokenTimeToLive: Duration) : TokenService {

    private val encoder = Base64.getUrlEncoder()
    private val decoder = Base64.getUrlDecoder()
    private val json = Jackson.newObjectMapper()

    init {
        json.registerKotlinModule()
    }

    /**
     * TODO actually sign tokens!! Currently, we just encode them.
     */
    override fun sign(token: Token): String =
            encoder.encodeToString(json.writeValueAsBytes(token))

    /**
     * Verify the token is signed and is valid.
     *
     * TODO actually verify the signature, currently any JWT token will be accepted.
     */
    override fun verify(encodedToken: String?): Token? {
        if (encodedToken == null) {
            return null
        } else try {
            val tokenString = decoder.decode(encodedToken).toString(StandardCharsets.UTF_8)
            return json.readValue(tokenString)
        } catch (e: Exception) {
            // invalid token
            return null
        }
    }

}

class MemoryBasedUserStorage : UserStorage {

    private val log = LoggerFactory.getLogger(MemoryBasedUserStorage::class.java)

    private val usersById = mutableMapOf<String, PersistedUser>()

    init {
        // For test purposes, add a couple of users to the map
        usersById["1111"] = PersistedUser(
                User("renato", "12"), "1111")

        usersById["2222"] = PersistedUser(
                User("john", "34"), "2222")
    }

    /**
     * This is a simple memory storage that does NOT CHECK THE PASSWORD!!!!
     */
    override fun getByCredentials(userCredentials: UserCredentials): PersistedUser? {
        log.warn("Checking if user exists, will not even look at the password: {}", userCredentials)
        return usersById.values.find { it.user.username == userCredentials.username }
    }

    override fun getById(userId: String): PersistedUser? = usersById[userId]

    override fun createUser(user: User,
                            plainTextPassword: String): PersistedUser {
        // TODO save user password?
        val duplicateUserName = usersById.values.filter { it.user.username == user.username }.firstOrNull()

        if (duplicateUserName != null) {
            throw StorageException("username already exists")
        }

        val persistedUser = PersistedUser(user, UUID.randomUUID().toString())
        usersById[persistedUser.userId] = persistedUser
        return persistedUser
    }

}

class MemoryBasedNonceStorage(val nonceTimeToLive: Duration) : NonceStorage {

    val log = LoggerFactory.getLogger(MemoryBasedNonceStorage::class.java)

    data class NonceData(val data: String, val expiresAt: Instant)

    private val dataByNonce = mutableMapOf<String, NonceData>()

    override fun createNonce(data: String): String {
        val nonce = UUID.randomUUID().toString()
        dataByNonce[nonce] = NonceData(data, Instant.now().plus(nonceTimeToLive))
        log.info("Created nonce {} for data {}", nonce, data)
        return nonce
    }

    override fun verifyNonce(nonce: String): String? {
        val nonceData = dataByNonce.remove(nonce)

        if (nonceData != null) {
            log.info("Nonce was validated, got data: {}", nonceData)

            if (Instant.now().isBefore(nonceData.expiresAt)) {
                return nonceData.data
            } else {
                log.info("Nonce has expired")
            }
        } else {
            log.info("Presented nonce is invalid")
        }

        return null
    }
}

class MemoryBasedLoginsStorage : LoginDataStorage {

    // implemenation only used for tests
    override fun getByUserName(username: String, limit: Int): List<LoginData> =
            listOf(
                    LoginData("renato", Instant.now().minus(Duration.ofSeconds(30))),
                    LoginData("renato", Instant.now().minus(Duration.ofDays(20)))
            )

    override fun insertFor(user: PersistedUser): Unit {
        TODO("not implemented")
    }
}