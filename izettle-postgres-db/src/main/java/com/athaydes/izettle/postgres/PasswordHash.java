package com.athaydes.izettle.postgres;


import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Password-hash manager.
 */
final class PasswordHash {

    // values set up to take around 200ms on a developer machine
    private static final int ITERATIONS = 4096 * 10;
    private static final int KEY_LENGTH = 255;
    private static final int SALT_LENGTH = 512;

    /**
     * Source: https://www.owasp.org/index.php/Hashing_Java
     *
     * @param password
     * @param salt
     * @param iterations
     * @param keyLength
     * @return hash
     */
    private static byte[] hashPassword( final char[] password,
                                        final byte[] salt,
                                        final int iterations,
                                        final int keyLength ) {

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            PBEKeySpec spec = new PBEKeySpec( password, salt, iterations, keyLength );
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded();
            return res;

        } catch ( NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException( e );
        }
    }

    static byte[] hash( String plainTextPassword, byte[] salt ) {
        return hashPassword( plainTextPassword.toCharArray(),
                salt, ITERATIONS, KEY_LENGTH );
    }

    static byte[] generateSalt() {
        byte[] salt = new byte[ SALT_LENGTH ];
        new SecureRandom().nextBytes( salt );
        return salt;
    }
}
