package com.athaydes.izettle.postgres;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersMapper implements ResultSetMapper<Users> {

    @Override
    public Users map( int index, ResultSet r, StatementContext ctx ) throws SQLException {
        Users users = new Users();
        users.setUserid( r.getString( "username" ) );
        users.setUsername( r.getString( "username" ) );
        users.setPhonenumber( r.getString( "phoneNumber" ) );
        users.setPassword( r.getBytes( "password" ) );
        users.setSalt( r.getBytes( "salt" ) );
        return users;
    }
}
