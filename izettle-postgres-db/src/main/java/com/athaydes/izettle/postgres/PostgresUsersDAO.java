package com.athaydes.izettle.postgres;

import org.postgresql.util.PSQLException;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

/**
 * Data Access Object for the users table.
 */
@RegisterMapper( UsersMapper.class )
public interface PostgresUsersDAO {

    @SqlQuery( "select * from users where username=:username" )
    Users getByUsername( @Bind( "username" ) String username )
            throws PSQLException;

    @SqlQuery( "select * from users where \"userId\"=:userId" )
    Users getById( @Bind( "userId" ) String userId )
            throws PSQLException;

    @SqlUpdate( "insert into users (username, \"phoneNumber\", password, salt) " +
            "values (:username, :phoneNumber, :password, :salt)" )
    void create( @Bind( "username" ) String username,
                 @Bind( "phoneNumber" ) String phone,
                 @Bind( "password" ) byte[] password,
                 @Bind( "salt" ) byte[] salt )
            throws PSQLException;

    @SqlQuery( "select \"userId\" from users where username=:username" )
    String getIdOfUser( @Bind( "username" ) String username )
            throws PSQLException;

}
